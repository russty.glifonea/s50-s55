import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

export default function CourseCard({course}){

	// Destructuring the props
	const {name, description, price, _id} = course

	// Initialize a 'count' state with a value of zero (0)
	const [count, setCount] = useState(0)
	// Initialize a 'slots' state with a value of 15 (15)
	const [slots, setSlots] = useState(15)
	// Initialize a 'isOpen' state with a value true
	const [isOpen, setIsOpen] = useState(true)

	// function enroll(){		
	// 	if (slots > 0) { 
	// 		setSlots(slots - 1)
	// 		setCount(count + 1)
	// 	} else {
	// 		alert("No more slots available.")
	// 	}
	// }

	// Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs.
	// You may also listen or watch a specific state for changes instead of watching/listening to the whole component.
	// useEffect(() => {
	// 	if(slots === 0){
	// 		setIsOpen(false)
	// 	}

	// }, [slots])

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				{/*<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Slots: {slots}</Card.Text>
				<Card.Text>Is Open: {isOpen ? 'Yes' : 'No'}</Card.Text>
				<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

