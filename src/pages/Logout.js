import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext'
import { useEffect, useContext } from 'react'

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext)
	// Using the context, clear the contents of the local storage 
	unsetUser()

	// An effct which removes the user email from the global user states that comes from the context
	useEffect(() => {
		setUser({
			id: null
		})
	}, [])

	return
	<Navigate to="/login"/>
	
}